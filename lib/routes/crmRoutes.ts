
import {Request, Response} from "express";
import { NextFunction } from "connect";
import {Client, Pool} from 'pg';


export class Routes {    

    
    public routes(app): void { 
        const pool = new Pool({
            user: 'postgres',
            host: 'localhost',
            database: 'ShoppingProject',
            password: 'asdasd',
            port: 5432,
          });
          
          
          const client = new Client({
            user: 'postgres',
            host: 'localhost',
            database: 'ShoppingProject',
            password: 'asdasd',
            port: 5432,
          });
          client.connect();

     
    
        app.get('/api/product', (req: Request, res: Response, next: NextFunction)=> {
            console.log("Status: "+res.statusCode);

            const text: string = 'SELECT product_id, product_name, product_type, product_price, img_path FROM Product LEFT JOIN Image on "imgFK" = image.img_id;';

          client.query(text, (err, response) => {
                if(err){
                    res.send(err.stack);
                }else{
                    res.send(response.rows);
                }

            });
        });
        
        app.get('/api/products', (req: Request, res: Response, next: NextFunction) => {
            console.log("Status: "+ res.statusCode);

                const text: string = 'SELECT product_id, product_name, product_type, product_price, product_description, img_path FROM Product LEFT JOIN Image on "imgFK" = image.img_id;';
                client.query(text, (err, response) => {
                if(err){
                    res.send(err.stack);
                }else{
                    res.send(response.rows);
                }
            });
        });

        app.post('/api/orders', (req: Request, res: Response, next: NextFunction)=>{
            console.log("Status: " + res.statusCode);
            
            const text: string = 'INSERT INTO OrderDetail(quantity, total, date_order) VALUES ($1, $2, $3) RETURNING *;'

            const values: [any, any, any] = [req.body.quantity, req.body.total, new Date()];
            
            console.log(req.body);
            console.log(values);

            client.query(text, values, (err, response) => {
                if(err){
                    res.send(err.stack);
                }else{
                    res.send(response.rows[0]);
                    res.end();
                }
            });
        });
        
        app.get('/api/products/:id', (req: Request, res: Response, next: NextFunction)=>{
            
            console.log(res.statusCode)
            
            const text = 'SELECT product_id, product_name, product_description, product_price, img_path FROM Product LEFT JOIN Image on "imgFK" = image.img_id WHERE product_id = $1;';
            const values = [req.params.id];

            client.query(text, values, (err, response)=>{
                if(err){
                    console.log(err.stack);   
                }else{
                    res.send(response.rows);
                }
            });

  
        });
    }
}